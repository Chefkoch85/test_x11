#files := main.cpp
files := *.cpp
appname := app

app: $(files)
	@echo "Release build"
#	@echo $^	#prints all prerequsite files
	@echo $?	#prints all newer prerequsite files
	g++ -Wall -lX11 -O3 -o $(appname) $(files)

d: $(files)
	@echo "Debug build"
	@echo $?
	g++ -Wall -g -Og -o d $(files)

clean:
	@echo "cleaning up"
	rm -f $(appname)
	rm -f d
	rm -f *.o
