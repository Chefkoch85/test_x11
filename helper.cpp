
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <cassert>
#include <cerrno>



void getSizeHint(Display* dspl, Window win,
		  int* minW, int* minH,
		  int* maxW, int* maxH)
{
  XSizeHints hints = {};
  long ret = 0;
  if (!XGetWMNormalHints(dspl, win, &hints, &ret))
  {
    printf("Error: XGetWMNormalHints()\n");
    return;
  }

  *minW = hints.min_width;
  *minH = hints.min_height;
  *maxW = hints.max_width;
  *maxH = hints.max_height;
}



volatile bool tick_flag = true;
void sig_handler(int signo)
{
  assert(signo == SIGALRM);
  tick_flag = true;
}
void setup_timer(int interval_ms)
{
  itimerval itv = {0};
  itv.it_interval.tv_usec = 1000 * interval_ms;
  itv.it_value.tv_usec = 1000 * interval_ms;

  signal(SIGALRM, sig_handler);

  auto res = setitimer(ITIMER_REAL, &itv, nullptr);
  printf("T:%d, to:%lu, err:%u\n", res, itv.it_value.tv_sec, errno);
}
void stop_timer()
{
  setitimer(ITIMER_REAL, nullptr, nullptr);
}
bool TickFlag()
{
  if (tick_flag)
  {
    tick_flag = false;
    return true;
  }
  return false;
}