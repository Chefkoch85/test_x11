
#include <stdio.h>
#include <stdlib.h>
#include <cassert>
#include <cstring>
#include <ctime>
#include <err.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include "helper.h"
#include "xwindow.h"

void tick(const CWindow& win, int *ys);
void draw(CWindow &win, int *ys);

int main(int argc, char** argv)
{
  auto win = CWindow(600, 600);
  if (win.ConnectDisplayServer())
    err(1, "Error: %s\n", "ConnectDisplayServer()");

  if (win.OpenWindow("A house with no.."))
    err(2, "Error: %s\n", "OpenWindow()");

  if (win.SetUserEvents())
    err(3, "Error: %s\n", "SetUserEvents()");

  win.SetSizeHint(600, 400, 1920, 1080);
  win.ToggleMaximize();

  setup_timer(30);


  int ys[] = { 0,   // 0:floor
               0,   // 1:left wall
               0,   // 2:right wall
               0,   // 3:roof
               0,   // 4:text 1
               620 }; // 5:text 2


  bool run = true;
  while (run)
  {
    while (win.EventsPending())
    {
      XEvent ev = win.NextEvent();
      if (ev.type != 12)
        printf("Event.type: %d;\n", ev.type);

      if (win.HandleDefEvents(&ev, &run))
        continue;

      switch (ev.type)
      {
      case MapNotify:
      {
        printf("MapNotify\n");
        break;
      }
      case ConfigureNotify:
      {
        printf("ConfigureNotify\n");
        XSetForeground(win.GetDisplay(), win.GetDefaultGC(), 0x00FF0000);		// aarrggbb
        break;
      }
      case Expose:
      {
        //printf("Expose\n");
        win.ClearWindow();

        draw(win, ys);

        win.Flush();
        break;
      }
      case ButtonPress:
      {
        printf("ButtonPress\n");
        break;
      }
      case KeyPress:
      {
        printf("KeyPress\n");
        win.SendCloseEvent();
        break;
      }
      } // switch (ev.type)
    } // while (XPending(dspl) > 0)

    if (TickFlag())
    {
      tick(win, ys);
    }
  } // while (run)

  win.CloseDisplay();
  return 0;
}

void tick(const CWindow& win, int *ys)
{
  static int step = 0;
  GC gc = win.GetDefaultGC();
  switch (step)
  {
    case 0:
      XSetForeground(win.GetDisplay(), gc, 0x00FFFFFF);		// aarrggbb
      if (ys[step] < 500)
        ys[step] += 8;
      else
        step++;
      break;
    case 1:
      if (ys[step] < 500)
        ys[step] += 8;
      else
        step++;
      break;
    case 2:
      if (ys[step] < 500)
        ys[step] += 8;
      else
        step++;
      break;
    case 3:
      if (ys[step] < 220)
        ys[step] += 4;
      else
        step++;
      break;
    case 4:
      XSetForeground(win.GetDisplay(), gc, 0x00FF0000);		// aarrggbb
      if (ys[step] < 120)
        ys[step] += 2;
      else
        step++;
      break;  
    case 5:
      if (ys[step] > 535)
        ys[step] -= 2;
      else
        step++;
      break;
    case 6:
      stop_timer();
      break;
  }

  win.ClearArea();
}

void draw(CWindow &win, int *ys)
{
  GC gc = win.GetDefaultGC();
  auto d = win.GetDisplay();
  auto w = win.GetWindow();

  static char text1[] = "A House with no Windows, ..";
  static char text2[] = "must be from a linux user :-)";

  XDrawLine(d, w, gc, 150, 0 + ys[0], 450, 0 + ys[0]);     // floor
  XDrawLine(d, w, gc, 150, -300 + ys[1], 150, 0 + ys[1]);  // left wall
  XDrawLine(d, w, gc, 450, -300 + ys[2], 450, 0 + ys[2]);  // right wall

  XDrawLine(d, w, gc, 120, 0 + ys[3], 300, -100 + ys[3]);  // roof left 
  XDrawLine(d, w, gc, 300, -100 + ys[3], 480, 0 + ys[3]);  // roof right

  XDrawString(d, w, gc, 220, -10 + ys[4], text1, strlen(text1));
  XDrawString(d, w, gc, 210, -10 + ys[5], text2, strlen(text2));
}