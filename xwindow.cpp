
#include "xwindow.h"
#include <cstdio>

CWindow::CWindow(int w, int h)
  : _winWidth(w), _winHeight(h)
{

}

bool CWindow::ConnectDisplayServer()
{
  _dspl = XOpenDisplay(nullptr);
  return !_dspl;
}
void CWindow::CloseDisplay()
{
  XCloseDisplay(_dspl);
}

bool CWindow::OpenWindow(const char* title, unsigned int backColor/*= 0xFF181818*/)
{
  if (MatchVisual())
  {
    printf("Error: %s\n", "No matching visual info!\n");
    return true;
  }

  auto attr = SetWinAttr(backColor);
  unsigned long attrMask = CWBackPixel | CWColormap | CWEventMask | CWBitGravity;

  int rootWin = DefaultRootWindow(_dspl);

  _win = XCreateWindow(
          _dspl,        // display connection
          rootWin,      // parent window
			    0, 0,                   // x, y
			    _winWidth, _winHeight,  // w, h
			    0, 
          _visinfo.depth,
          InputOutput,
          _visinfo.visual,
			    attrMask, 
          &attr);
  
  if (!_win)
  {
    printf("Error: %s\n", "Could not create window!\n");
    return true;
  }

  if (title)
    SetTitle(title);

  XMapWindow(_dspl, _win);  // show window
  
  XFlush(_dspl);   // send all commands to server
  return false;
}
void CWindow::CloseWindow()
{
  XUnmapWindow(_dspl, _win);
  XDestroyWindow(_dspl, _win);
}

bool CWindow::SetUserEvents()
{
  WM_DELETE_WIN = XInternAtom(_dspl, "WM_DELETE_WINDOW", false);
  if (!XSetWMProtocols(_dspl, _win, &WM_DELETE_WIN, 1))
  {
    printf("Error: %s\n", "XSetWMProtocols() failed!\n");
    return true;
  }
  return false;
}

bool CWindow::EventsPending() const
{
  return XPending(_dspl) > 0;
}
XEvent CWindow::NextEvent() const
{
  XEvent ev = {};
  XNextEvent(_dspl, &ev);
  return ev;
}
bool CWindow::HandleDefEvents(XEvent* ev, bool *run)
{
  switch (ev->type)
  {
  case DestroyNotify:
  {
    printf("DestroyNotify\n");
    XDestroyWindowEvent* e = (XDestroyWindowEvent*)ev;
    if (IsThisWindow(e->window))
    {
      CloseWindow();
      *run = false;
    }
    return true;
  }
  case ClientMessage:
  {
    printf("ClientMessage\n");
    XClientMessageEvent* e = (XClientMessageEvent*)ev;
    if ((Atom)e->data.l[0] == WM_DELETE_WIN)
    {
      SendCloseEvent();
      return true;
    }
    break;
  }
  }

  return false;
}

void CWindow::SendCloseEvent() const
{
  XEvent se = {0};
  se.type = DestroyNotify;
  ((XDestroyWindowEvent*)&se)->window = _win;
  XSendEvent(_dspl, _win, false, StructureNotifyMask, &se);
}
void CWindow::SendExposeEvent() const
{	
  XEvent se = {0};
  se.type = Expose;
  ((XExposeEvent*)&se)->window = _win;
  XSendEvent(_dspl, _win, false, ExposureMask, &se);
}


void CWindow::SetTitle(const char* title)
{
  XStoreName(_dspl, _win, title);
}

void CWindow::SetSizeHint(int minw, int minh, int maxw, int maxh)
{
  XSizeHints hints = {};
  if (minw > 0 && minh > 0) hints.flags |= PMinSize;
  if (maxw > 0 && maxh > 0) hints.flags |= PMaxSize;

  hints.min_width = minw;
  hints.min_height = minh;
  hints.max_width = maxw;
  hints.max_height = maxh;

  XSetWMNormalHints(_dspl, _win, &hints);
}

Status CWindow::ToggleMaximize()
{
  XClientMessageEvent ev = {};

  Atom wmState = XInternAtom(_dspl, "_NET_WM_STATE", false);
  Atom maxH    = XInternAtom(_dspl, "_NET_WM_MAXIMIZED_HORZ", false);
  Atom maxV    = XInternAtom(_dspl, "_NET_WM_MAXIMIZED_VERT", false);

  if (wmState == None)
  {
    printf("Error: XInternAtom(dspl, \"_NET_WM_STATE\", false)");
    return 1;
  }

  ev.type = ClientMessage;
  ev.display = _dspl;
  ev.format = 32;
  ev.window = _win;
  ev.message_type = wmState;
  ev.data.l[0] = 2; //_NET_WM_STATE_TOGGLE;
  ev.data.l[1] = maxH;
  ev.data.l[2] = maxV;
  ev.data.l[3] = 1;

  return XSendEvent(_dspl, DefaultRootWindow(_dspl), False, SubstructureNotifyMask, (XEvent*)&ev);
}

void CWindow::ClearArea(bool expose/*= true*/) const
{
  ClearArea(0, 0, _winWidth, _winHeight, expose);
}
void CWindow::ClearArea(int x, int y, int w, int h, bool expose/*= true*/) const
{
  XClearArea(_dspl, _win, x, y, w, h, expose);
}

void CWindow::ClearWindow() const
{
  XClearWindow(_dspl, _win);
}

void CWindow::Flush()
{
  XFlush(_dspl);
}


bool CWindow::IsThisWindow(Window w) const
{
  return w == _win;
}


int CWindow::GetDefaultScreen() const
{
  return DefaultScreen(_dspl);
}
int CWindow::GetRootWindow() const
{
  return DefaultRootWindow(_dspl);
}
GC CWindow::GetDefaultGC() const
{
  return DefaultGC(_dspl, DefaultScreen(_dspl));
}


//*** private ***//
bool CWindow::MatchVisual()
{
  int defScrn = DefaultScreen(_dspl);

  int scrnBitDepth = 24;
  if (!XMatchVisualInfo(_dspl, defScrn, scrnBitDepth, TrueColor, &_visinfo))
  {
    return true;
  }
  return false;
}

XSetWindowAttributes CWindow::SetWinAttr(unsigned int backColor)
{
  int rootWin = DefaultRootWindow(_dspl);
  
  XSetWindowAttributes winAttr;
  winAttr.event_mask = 
    StructureNotifyMask |
    ExposureMask |
    ButtonPressMask |
    ButtonReleaseMask |
    KeyPressMask |
    KeyReleaseMask;

  winAttr.bit_gravity = StaticGravity;
  winAttr.background_pixel = backColor;
  winAttr.colormap = XCreateColormap(_dspl, rootWin, _visinfo.visual, AllocNone);
  return winAttr;
}