
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

class CWindow
{
public:
  Atom WM_DELETE_WIN;

public:
  CWindow(int w, int h);
  bool ConnectDisplayServer();
  bool OpenWindow(const char* title, unsigned int backColor = 0xFF181818);
  void CloseWindow();
  void CloseDisplay();

  bool SetUserEvents();

  bool EventsPending() const;
  XEvent NextEvent() const;
  bool HandleDefEvents(XEvent* ev, bool *run);

  void SendCloseEvent() const;
  void SendExposeEvent() const;


  void SetTitle(const char* title);
  void SetSizeHint(int minw, int minh, int maxw, int maxh);
  Status ToggleMaximize();
  void ClearArea(bool expose = true) const;
  void ClearArea(int x, int y, int w, int h, bool expose = true) const;
  void ClearWindow() const;
  void Flush();

  int GetDefaultScreen() const;
  int GetRootWindow() const;
  GC GetDefaultGC() const;
  inline Display* GetDisplay() const { return _dspl; }
  inline Window GetWindow() const { return _win; }

  inline int GetWidth() const { return _winWidth; }
  inline int GetHeight() const { return _winHeight; }

  bool IsThisWindow(Window w) const;

private:
  Display* _dspl = nullptr;
  Window _win = 0;
  XVisualInfo _visinfo = {};
  int _winWidth = 0, _winHeight = 0;

  bool MatchVisual();
  XSetWindowAttributes SetWinAttr(unsigned int backColor);
};